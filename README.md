# Installation

1. Install ruby.
2. Execute `bundle install`
3. Execute `bundle exec middleman` to start a server on http://localhost:4567
4. Execute `bundle exec middleman build` to build a production version

The Middleman configuration file is `config.rb` and template configuration can be found in `data` folder.
The front matter can be in YAML (denoted by `---`) or JSON (`;;;`)
