require "builder";
set :markdown_engine, :redcarpet
set :markdown, :fenced_code_blocks => true, :smartypants => true
set :redcarpet, autolink: true

# Change Compass configuration
compass_config do |config|
  config.output_style = :compact
end

page "/sitemap.xml", :layout => false

###
# Helpers
###

# Automatic image dimensions on image_tag helper
activate :automatic_image_sizes

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

activate :disqus do |d|
  d.shortname = 'underplan'
end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

activate :directory_indexes

activate :middleman_simple_thumbnailer
#activate :imageoptim

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :asset_hash

  activate :minify_html

  activate :gzip

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end

activate :deploy do |deploy|
  deploy.method   = :sftp
  deploy.host     = 'oreolek.ru'
  deploy.path     = '/var/www/underplan'
  deploy.build_before = true
  deploy.user = data.deploy.user
  deploy.password = data.deploy.password
end
